rm_theme
roromedia base theme for drupal
------------------------------------------------------------------------------
found at https://bitbucket.org/roromediateam/rm_theme

License
------------------------------------------------------------------------------
rm_theme is licensed under GPL v2 (http://www.gnu.org/licenses/gpl-2.0.html)

Author - Markus Oberlehner
------------------------------------------------------------------------------
Email: markus.oberlehner@gmail.com
Twitter: https://twitter.com/MaOberlehner
Work: Web Developer @ roromedia - http://www.roromedia.com

Thanks
------------------------------------------------------------------------------
Harry Roberts (@csswizardry)
A big thank you to Harry Roberts for his great work
https://github.com/csswizardry/inuit.css
https://github.com/csswizardry/CSS-Guidelines