jQuery(function($) {

  var $body = $('body'),
      $main_menu = $('#block--main-menu'),
      $main_menu__show_nav_wrap = $('<div class="block--main-menu__show-nav-wrap" />'),
      $main_menu__show_nav = $('<p class="block--main-menu__show-nav"><i class="block--main-menu__show-nav__icon fa fa-bars"></i><span class="block--main-menu__title">Navigation</span></p>'),
      $main_menu__hide_nav = $('<div class="block--main-menu__hide-nav"><i class="block--main-menu__hide-nav__icon fa fa-times-circle-o"></i></div>');

  // iPad Kompatibilität
  $('.menu').on('touchstart', '.nolink', function() {
    $('.menu-link--touched').removeClass('menu-link--touched');
    $(this).addClass('menu-link--touched');
  });

  $.rc({
    max: Drupal.settings.rm_grid_breakpoint_s - 1,
    run_on_enter: function() {
      // off canvas menu aktivieren
      $body.prepend($main_menu__show_nav_wrap);
      $main_menu.prepend($main_menu__hide_nav);
    },
    run_once_on_enter: function() {
      // off canvas menu aktivieren
      $main_menu__show_nav_wrap.append($main_menu__show_nav);

      $main_menu__show_nav.on('click', function() {
        $body.toggleClass('main-menu-active');
      });

      $main_menu__hide_nav.on('click', function() {
        $body.removeClass('main-menu-active');
      });
    },
    run_on_leave: function() {
      // off canvas menu deaktivieren
      $main_menu__show_nav_wrap.detach();
      $main_menu__hide_nav.detach();

      $body.removeClass('main-menu-active');
    }
  });
});