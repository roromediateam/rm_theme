<?php

function STARTERKIT_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    // HTML5 placeholder attribute
    $form['search_block_form']['#attributes']['placeholder'] = t('Ihr Suchbegriff') . '...';
  }
}

/**
 * Add class menu--horizontal or menu--vertical
 */
function STARTERKIT_menu_tree__main_menu(&$vars) {
  return '<ul class="menu menu--horizontal clearfix">' . $vars['tree'] . '</ul>';
}