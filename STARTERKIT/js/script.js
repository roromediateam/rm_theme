jQuery(function($) {

  /* colorbox */
  if ($.colorbox) {
    $.rc({
      max: Drupal.settings.rm_grid_breakpoint_s - 1,
      run_on_enter: function() {
        $.colorbox.remove();
      }
    });
  }

  /**
   * Slideshow
   */
  var $slideshow_holder = $('#region--slideshow__holder'),
      slideshow_flexslider_html,
      defer_slideshow_assets = new $.Deferred(),
      defer_slideshow_html = new $.Deferred(),
      defer_slideshow_all = [
        defer_slideshow_assets,
        defer_slideshow_html
      ],
      defer_slideshow = $.when.apply($, defer_slideshow_all);

  // run when all ajax calls are done
  defer_slideshow.done(function() {
    $slideshow_holder.html(slideshow_flexslider_html);
    $slideshow_holder.find('#slideshow-flexslider').addClass('is-active');

    // init flexslider
    slideshow_flexslider_init();
  });

  // load flexslider assets
  yepnope({
    load: [
      '/sites/all/libraries/flexslider/jquery.flexslider-min.js',
      '/sites/all/libraries/flexslider_base_theme/flexslider.base.css'
    ],
    complete: function () {
      defer_slideshow_assets.resolve();
    }
  });

  $.rc({
    min: Drupal.settings.rm_grid_breakpoint_s,
    max: Drupal.settings.rm_grid_breakpoint_m,
    // small
    run_on_leave_to_bottom: function () {
      slideshow_flexslider_view_load_display('small');
    },
    // medium
    run_on_enter: function () {
      slideshow_flexslider_view_load_display('medium');
    },
    // large
    run_on_leave_to_top: function () {
      slideshow_flexslider_view_load_display('large');
    }
  });

  function slideshow_flexslider_init () {
    $('#slideshow-flexslider').flexslider({
      slideshow: true,
      animation: 'slide',
      slideshowSpeed: 70000,
      animationSpeed: 400,
      controlNav: true,
      directionNav: true,
      pauseOnHover: true,
      prevText: '<i class="fa fa-angle-left fa-3x"></i>',
      nextText: '<i class="fa fa-angle-right fa-3x"></i>'
    });
  }

  // load the defined display from the slideshow_flexslider view
  function slideshow_flexslider_view_load_display (display_id) {
    $.ajax({
      url: '/views/ajax',
      type: 'GET',
      data: {
        'view_name': 'slideshow_flexslider',
        'view_display_id': display_id,
        'view_path': Drupal.settings.slideshow_flexslider.path
      },
      success: function(response) {
        // save the data and trigger html loaded event
        slideshow_flexslider_html = response[1].data;
        defer_slideshow_html.resolve();
      },
      error: function(xhr) {
        console.log(xhr);
      },
      dataType: 'json'
    });
  }
});