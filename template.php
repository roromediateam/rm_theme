<?php
/**
 * @file
 * Contains functions to alter Drupal's markup for the rm_theme.
 *
 * IMPORTANT WARNING: DO NOT MODIFY THIS FILE.
 *
 * The base rm_theme is designed to be easily extended by its sub-themes. You
 * shouldn't modify this or any of the CSS or PHP files in the root rm_theme/ folder.
 */


/**
 * Implements hook_form_alter().
 */
function rm_theme_form_alter(&$form, &$form_state, $form_id) {
  // add .button classes to submit and reset buttons
  if (isset($form['actions'])) {
    $form['actions']['submit']['#attributes']['class'][] = 'button';
    $form['actions']['reset']['#attributes']['class'][] = 'button';
  }

  if (isset($form['submit'])) {
    $form['submit']['#attributes']['class'][] = 'button';
  }
}

/**
* Implements template_preprocess_html().
*/
function rm_theme_preprocess_html(&$vars) {
  // include modernizr and respondjs
  // TODO!!!! only load if necessary
  $vars['modernizr'] = '<script src="' . base_path() . libraries_get_path('modernizr') . '/modernizr.min.js"></script>';
  $vars['respondjs'] = '<script src="' . base_path() . libraries_get_path('respondjs') . '/dest/respond.min.js"></script>';

  foreach ($vars['classes_array'] as $k => $v) {
    // remove page-node- class from body
    if ($v == 'page-node-') {
      unset($vars['classes_array'][$k]);
      continue;
    }

    // transform classes to BEM style
    if (strpos($v, 'page-node-') !== FALSE) {
      $vars['classes_array'][$k] = str_replace('page-node-', 'page-node--', $v);
    }
  }
}

/**
* Implements template_preprocess_page().
*/
function rm_theme_preprocess_page(&$vars, $hook) {
  global $theme, $user;

  // include newer version of jQuery
  $jquery = theme_get_setting('jquery');
  $jquery_path = '/jquery-' . $jquery['version'] . '.min.js';
  drupal_add_js(libraries_get_path('jquery') . $jquery_path, array('weight' => -99, 'every_page' => TRUE, 'version' => $jquery['version']));

  // add jQuery rc plugin
  $jquery_rc_path = '/jquery.rc.js';
  drupal_add_js(libraries_get_path('rc') . $jquery_rc_path, array('weight' => -98, 'every_page' => TRUE));

  // other js files
  drupal_add_js(drupal_get_path('theme', 'rm_theme') . '/js/console.js', array('weight' => -100, 'every_page' => TRUE));
  drupal_add_js(drupal_get_path('theme', $theme) . '/js/config.js', array('weight' => 0, 'every_page' => TRUE));
  drupal_add_js(drupal_get_path('theme', $theme) . '/js/script.js', array('weight' => 100, 'every_page' => TRUE));
  drupal_add_js(drupal_get_path('theme', 'rm_theme') . '/js/menu.js', array('every_page' => TRUE));
  drupal_add_js(drupal_get_path('theme', 'rm_theme') . '/js/skiplinks.js', array('every_page' => TRUE));

  // font-awesome
  drupal_add_css(libraries_get_path('font-awesome') . '/css/font-awesome.min.css', array('group' => CSS_THEME, 'weight' => 9998, 'every_page' => TRUE));

  // styles for admin ui elements
  if ($user->uid) {
    drupal_add_css(drupal_get_path('theme', 'rm_theme') . '/css/ui/tabs.css', array('group' => CSS_THEME, 'weight' => 9998, 'every_page' => TRUE));
  }

  // main scaffolding style file
  drupal_add_css(drupal_get_path('theme', $theme) . '/less/scaffolding.less', array('group' => CSS_THEME, 'weight' => 9999, 'every_page' => TRUE));

  // various meta elements for the html head
  $head_data = array();
  $webfont_counter = 1;
  $webfonts = theme_get_setting('fonts');
  // add webfonts defined in template.info file
  if (is_array($webfonts)) {
    foreach ($webfonts as $webfont) {
      $head_data['webfont_' . $webfont_counter] = array(
        '#tag' => 'link',
        '#attributes' => array(
          'href' => $webfont,
          'rel' => 'stylesheet',
        )
      );

      $webfont_counter++;
    }
  }
  // deactivate automatic iOS / Android phone number detection
  $head_data['tel_detection'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'format-detection',
      'content' => 'telephone=no',
    )
  );
  // define viewport
  $head_data['viewport'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width,initial-scale=1',
    )
  );
  // deactivate IE compatibility mode
  $head_data['http_equiv'] = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge,chrome=1',
    ),
    '#weight' => '-9999',
  );
  // add elements to the head
  foreach ($head_data as $key => $data) {
    drupal_add_html_head($data, $key);
  }

  // define item widths for content and sidebars
  $grid = theme_get_setting('grid');
  $content_item = $grid['columns'];

  if(!empty($vars['page']['sidebar1'])) {
    $content_item = $content_item - $grid['sidebar1'];
  }

  if(!empty($vars['page']['sidebar2'])) {
    $content_item = $content_item - $grid['sidebar2'];
  }

  $vars['page']['content_item'] = $content_item;
  $vars['page']['sidebar1_item'] = $grid['sidebar1'];
  $vars['page']['sidebar2_item'] = $grid['sidebar2'];
}

/**
 * Implements hook_html_head_alter().
 */
function rm_theme_html_head_alter(&$head) {
  // simplify the meta tag for character encoding
  if (isset($head['system_meta_content_type']['#attributes']['content'])) {
    $head['system_meta_content_type']['#attributes'] = array('charset' => str_replace('text/html; charset=', '', $head['system_meta_content_type']['#attributes']['content']));
  }

  /**
   * Show correct path information
   * on multiple meta elements for front page
   *
   * https://drupal.org/node/1316006
   */
  if (drupal_is_front_page()) {
    if (isset($head['metatag_canonical'])) {
      $head['metatag_canonical']['#value'] = url(NULL, array('absolute' => TRUE));
    }

    if (isset($head['metatag_shortlink'])) {
      $head['metatag_shortlink']['#value'] = url(NULL, array('absolute' => TRUE));
    }

    if (isset($head['rdf_node_comment_count'])) {
      $head['rdf_node_comment_count']['#attributes']['about'] = '/';
    }

    if (isset($head['rdf_node_title'])) {
      $head['rdf_node_title']['#attributes']['about'] = '/';
    }
  }

  /**
   * Remove useless generator metatag
   */
  if (isset($head['metatag_generator'])) {
    unset($head['metatag_generator']);
  }
}

/**
 * Implements hook_js_alter().
 */
function rm_theme_js_alter(&$js) {
  global $user;

  $rm_theme_path = drupal_get_path('theme', 'rm_theme');

  foreach ($js as $k => $v) {
    // remove old version of jquery
    if (strpos($k, 'misc/jquery.js') !== FALSE || strpos($k, 'jquery_update') !== FALSE) {
      unset($js[$k]);
    }

    // original jquery.form.js not compatible with jquery >= 1.8
    if (strpos($k, 'misc/jquery.form.js') !== FALSE) {
      // remove old version
      unset($js[$k]);
      // include new version
      drupal_add_js($rm_theme_path . '/js/fix/jquery.form.js');
    }

    // original admin_menu.js not compatible with jquery >= 1.8
    if (strpos($k, drupal_get_path('module', 'admin_menu') . '/admin_menu.js') !== FALSE) {
      // remove old version
      unset($js[$k]);
      // include new version
      drupal_add_js($rm_theme_path . '/js/fix/admin_menu.js');
    }

    // original jquery.drilldown.js not compatible with jquery >= 1.8
    if (strpos($k, drupal_get_path('module', 'admin') . '/includes/jquery.drilldown.js') !== FALSE) {
      // remove old version
      unset($js[$k]);
      // include new version
      drupal_add_js($rm_theme_path . '/js/fix/jquery.drilldown.js');
    }
  }
}

/**
* Implements template_preprocess_views_view().
*/
function rm_theme_preprocess_views_view(&$vars) {
  $view_name = str_replace('_', '-', $vars['view']->name);
  $view_display = str_replace('_', '-', $vars['view']->current_display);

  $old_class_pattern = array(
    'view-',
    '--' . $view_name,
    '-id-',
    '--display-id',
    '_',
  );
  $new_class_pattern = array(
    'view--',
    '--' . $view_name . '--' . $view_display,
    '-id--',
    '--display',
    '-',
  );

  foreach ($vars['classes_array'] as $k => $class) {
    // remove dom-id class
    if (strpos($class, 'dom-id') !== FALSE) {
      unset($vars['classes_array'][$k]);
      continue;
    }
    // BEM style classes
    $vars['classes_array'][$k] = str_replace($old_class_pattern, $new_class_pattern, $class);
  }

  // css display
  $vars['css_display'] = $view_display;
}

/**
* Implements template_preprocess_views_view_unformatted().
*/
function rm_theme_preprocess_views_view_unformatted(&$vars) {
  $view_name = str_replace('_', '-', $vars['view']->name);
  $view_display = str_replace('_', '-', $vars['view']->current_display);

  $old_class_pattern = array(
    'views-',
    'row-',
    'grid--',
  );
  $new_class_pattern = array(
    'view__',
    'row--',
    'grid__',
  );

  foreach ($vars['classes_array'] as $k => $class) {
    // BEM style classes
    $vars['classes_array'][$k] =
      str_replace($old_class_pattern, $new_class_pattern, $class) . ' ' .
      // add BEM style unique class
      'view--' . $view_name . '--' . $view_display . '__row';
  }
}

/**
* Implements template_preprocess_views_view_fields().
*/
function rm_theme_preprocess_views_view_fields(&$vars) {
  $old_class_pattern = array(
    '-field-title',
    '-field-body',
    '-field-field',
    'views-field',
    'views',
  );
  $new_class_pattern = array(
    '__field--title',
    '__field--body',
    '__field--field',
    'view__field',
    'view',
  );

  foreach ($vars['fields'] as $k => $field) {
    $vars['fields'][$k]->wrapper_prefix = str_replace($old_class_pattern, $new_class_pattern, $field->wrapper_prefix);

    $field_name = str_replace('_', '-', $k);

    $vars['fields'][$k]->content = str_replace(
      'class="field-content"',
      'class="view__field__content view__field--' . $field_name . '__content"',
      $field->content
    );

    $vars['fields'][$k]->label_html = str_replace(
      'class="views-label views-label-',
      'class="view__label view__label--',
      $field->label_html
    );
  }
}

/**
 *
 */
function rm_theme_preprocess_field(&$vars, $hook) {
  // BEM Style field classes
  foreach ($vars['classes_array'] as $k => $class) {
    $vars['classes_array'][$k] = str_replace(
      array('field-'),
      array('field--'),
      $class
    );
  }

  if ($vars['element']['#entity_type'] == 'node') {
    $vars['classes_array'][] = 'field--node-' . $vars['element']['#bundle'] . '--' . str_replace(array('field_', '_'), array('', '-'), $vars['element']['#field_name']);
  }
}

function rm_theme_field($vars) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$vars['label_hidden']) {
    $output .= '<div class="field__label"' . $vars['title_attributes'] . '>' . $vars['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field__items"' . $vars['content_attributes'] . '>';

  foreach ($vars['items'] as $delta => $item) {
    $classes = 'field__item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $vars['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }

  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $vars['classes'] . '"' . $vars['attributes'] . '>' . $output . '</div>';

  return $output;
}

/**
 * style und script tags optimieren
 */
function rm_theme_process_html_tag(&$vars) {
  $tag = &$vars['element'];

  if ($tag['#tag'] === 'style' || $tag['#tag'] === 'script') {
    // Remove redundant type attribute and CDATA comments.
    unset($tag['#attributes']['type'], $tag['#value_prefix'], $tag['#value_suffix']);

    // Remove media="all" but leave others unaffected.
    if (isset($tag['#attributes']['media']) && $tag['#attributes']['media'] === 'all') {
      unset($tag['#attributes']['media']);
    }
  }
}

/**
 * HTML Output der Navigation optimieren
 */
function rm_theme_menu_link(&$vars) {
  $element = $vars['element'];
  $sub_menu = '';

  /* li elements */
  foreach ($element['#attributes']['class'] as $k => $class) {
    switch ($class) {
      case 'leaf':
        unset($element['#attributes']['class'][$k]);
        break;

      case 'expanded':
        $element['#attributes']['class'][$k] = 'menu__item--expanded';
        break;
    }
  }

  $menu_id = 'menu__item--' . $element['#original_link']['mlid'];

  $element['#attributes']['id'][] = $menu_id;
  $element['#attributes']['class'][] = 'menu__item';
  $element['#attributes']['class'][] = 'block--' . $element['#original_link']['menu_name'] . '__menu__item';
  $element['#attributes']['class'][] = 'menu__item--level' . $element['#original_link']['depth'];
  $element['#attributes']['class'][] = 'block--' . $element['#original_link']['menu_name'] . '__menu__item--level' . $element['#original_link']['depth'];
  $element['#attributes']['class'][] = $menu_id;

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  if ($element['#href'] == '<front>' && drupal_is_front_page()) {
    $element['#attributes']['class'][] = 'active';
  }

  /* a or span (nolink) elements */
  $link_id = 'menu__link--' . $element['#original_link']['mlid'];

  $element['#localized_options']['attributes']['id'][] = $link_id;
  $element['#localized_options']['attributes']['class'][] = 'menu__link';
  $element['#localized_options']['attributes']['class'][] = 'block--' . $element['#original_link']['menu_name'] . '__menu__link';
  $element['#localized_options']['attributes']['class'][] = 'menu__link--level' . $element['#original_link']['depth'];
  $element['#localized_options']['attributes']['class'][] = 'block--' . $element['#original_link']['menu_name'] . '__menu__link--level' . $element['#original_link']['depth'];
  $element['#localized_options']['attributes']['class'][] = $link_id;

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . '</li>';
}

function rm_theme_menu_tree(&$vars) {
  return '<ul class="menu clearfix">' . $vars['tree'] . '</ul>';
}

/**
 * Accesibility und HTML Output optimieren
 */
function rm_theme_preprocess_block(&$vars, $hook) {
  $vars['html_tag'] = 'div';

  // Use a template with no wrapper for the page's main content.
  if ($vars['block_html_id'] == 'block-system-main') {
    $vars['theme_hook_suggestions'][] = 'block__no_wrapper';
  }

  // BEM Style id and classes
  $vars['block_html_id'] = str_replace(
    array('block-' . str_replace('_', '-', $vars['block']->module . '-' . $vars['block']->delta)),
    array('block--' . str_replace('_', '-', $vars['block']->delta)),
    $vars['block_html_id']
  );

  foreach ($vars['classes_array'] as $k => $class) {
    $vars['classes_array'][$k] = str_replace('block-', 'block--', $class);
  }

  $vars['classes_array'][] = $vars['block_html_id'];

  $vars['title_attributes_array']['class'][] = 'title';
  $vars['title_attributes_array']['class'][] = 'title--block';
  $vars['title_attributes_array']['class'][] = $vars['block_html_id'] . '__title';

  // Add Aria Roles via attributes.
  switch ($vars['block']->module) {
    case 'system':
      switch ($vars['block']->delta) {
        case 'main':
          // Note: the "main" role goes in the page.tpl, not here.
          break;
        case 'help':
        case 'powered-by':
          $vars['attributes_array']['role'] = 'complementary';
          break;
        default:
          // Any other "system" block is a menu block.
          $vars['attributes_array']['role'] = 'navigation';
          $vars['html_tag'] = 'nav';
          break;
      }
      break;
    case 'menu':
    case 'menu_block':
    case 'blog':
    case 'book':
    case 'comment':
    case 'forum':
    case 'shortcut':
    case 'statistics':
      $vars['attributes_array']['role'] = 'navigation';
      $vars['html_tag'] = 'nav';
      break;
    case 'search':
      $vars['attributes_array']['role'] = 'search';
      break;
    case 'help':
    case 'aggregator':
    case 'locale':
    case 'poll':
    case 'profile':
      $vars['attributes_array']['role'] = 'complementary';
      break;
    case 'node':
      switch ($vars['block']->delta) {
        case 'syndicate':
          $vars['attributes_array']['role'] = 'complementary';
          break;
        case 'recent':
          $vars['attributes_array']['role'] = 'navigation';
          $vars['html_tag'] = 'nav';
          break;
      }
      break;
    case 'user':
      switch ($vars['block']->delta) {
        case 'login':
          $vars['attributes_array']['role'] = 'form';
          break;
        case 'new':
        case 'online':
          $vars['attributes_array']['role'] = 'complementary';
          break;
      }
      break;
  }
}