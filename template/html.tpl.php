<!DOCTYPE html>
<html class="no-js" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <script>if((window.devicePixelRatio===undefined?1:window.devicePixelRatio)>1)document.cookie='HTTP_IS_RETINA=1;path=/';</script>
  <?php print $styles; ?>
  <?php print $modernizr; ?>
  <!--[if lt IE 9]>
    <?php print $respondjs; ?>
  <![endif]-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <nav class="skiplinks" role="navigation">
    <ul>
      <li><a id="skiplinks__skiplink--main" href="#main-content" class="skiplinks__skiplink visuallyhidden focusable"><?php print t('Skip to main content'); ?></a></li>
    </ul>
  </nav>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
  <?php print $scripts; ?>
</body>
</html>